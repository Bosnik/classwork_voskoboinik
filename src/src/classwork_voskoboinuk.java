import java.util.Arrays;
import java.util.Random;

import static com.intellij.util.ArrayUtil.mergeArrayAndCollection;
import static com.intellij.util.ArrayUtil.swap;

public class classwork_voskoboinuk {

    public static void main(String[] args) {
        int[]arr = { 9, 2, 4, 7, 3, 7, 10 };
        int from = 0;
        int to = arr.length - 1;
        classwork_voskoboinuk quickSort = new classwork_voskoboinuk();
        quickSort.sort(arr);
        System.out.println(Arrays.toString(arr));

    }
    public void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    private void sort(int[] arr, int from, int to) {
        if (to - from <= 1) return;
        int pivot = partition(arr, from, to);
        sort(arr, from, pivot);
        sort(arr, pivot + 1, to);
    }

    public int partition(int[] arr, int from, int to) {
        int pivot = arr[from];
        int i = from + 1;
        int j = from + 1;
        while (j < to) {
            if (arr[j] < pivot) {
                swap(arr, i, j);
                i++;
            }
            j++;
        }
        swap(arr,from,i-1);
        return i-1;
    }
    private void swap(int[]arr,int i, int j){
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
        }

    }


