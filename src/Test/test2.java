import org.junit.*;

import java.util.Random;

import static junit.framework.TestCase.assertTrue;

public class test2 {

    @Test
    public void testPartition(){
        Random rand = new Random();
        int[]arr=new int[20];
        for (int i=0;i<arr.length;i++){
            arr[i]=rand.nextInt()%50;
        }
        classwork_voskoboinuk quickSort = new classwork_voskoboinuk();
        int pivot = quickSort.partition(arr,0,arr.length);

        for (int i = 0;i<pivot;i++){
            assertTrue(arr[i]<arr[pivot]);
        }
        for (int i = pivot;i<arr.length;i++){
            assertTrue(arr[i]>=arr[pivot]);
        }

    }@Test
    public void testSort(){
        Random rand = new Random();
        int[]arr=new int[20];
        for (int i=0;i<arr.length;i++){
            arr[i]=rand.nextInt()%50;
        }
        classwork_voskoboinuk quickSort = new classwork_voskoboinuk();
        quickSort.sort(arr);


        for (int i = 0;i<arr.length-1;i++){
            assertTrue(arr[i]<=arr[i+1]);
        }

    }




}